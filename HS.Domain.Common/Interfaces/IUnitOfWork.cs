﻿namespace HS.Domain.Common
{
    using System;
    using System.Threading;
    using System.Threading.Tasks;

    public interface IUnitOfWork : IDisposable
	{
		IGenericRepository GenericRepository { get; }
		IReadOnlyRepository ReadOnlyRepository { get; }

		void BeginTransaction();
		Task BeginTransactionAsync(CancellationToken token = default);

		void Save();
		Task SaveAsync(CancellationToken token = default);

		void Commit();
		Task CommitAsync(CancellationToken token = default);

		void Rollback();
		Task RollbackAsync(CancellationToken token = default);
	}
}