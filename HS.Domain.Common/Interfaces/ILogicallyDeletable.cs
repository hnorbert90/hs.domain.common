﻿namespace HS.Domain.Common
{
    using System;

    public interface ILogicallyDeletable
    {
        bool IsDeleted { get; set; }
        DateTime? DeletedOn { get; set; }
    }
}