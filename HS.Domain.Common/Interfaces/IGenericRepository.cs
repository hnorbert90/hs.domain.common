﻿namespace HS.Domain.Common
{
    using System;
    using System.Collections.Generic;
    using System.Linq.Expressions;
    using System.Threading;
    using System.Threading.Tasks;

    public interface IGenericRepository : IReadOnlyRepository, IRepository
	{
		void Create<TEntity>(TEntity entity) where TEntity : class, new();
		Task CreateAsync<TEntity>(TEntity entity, CancellationToken token = default) where TEntity : class, new();
		void CreateMany<TEntity>(IEnumerable<TEntity> entities) where TEntity : class, new();
		Task CreateManyAsync<TEntity>(IEnumerable<TEntity> entities, CancellationToken token = default) where TEntity : class, new();
		void Update<TEntity>(TEntity entity) where TEntity : class, new();
		void Update<TEntity>(Expression<Func<TEntity, TEntity>> newValues, Expression<Func<TEntity, bool>> where = null) where TEntity : class, new();
		Task UpdateAsync<TEntity>(Expression<Func<TEntity, TEntity>> newValues, Expression<Func<TEntity, bool>> where = null, CancellationToken token = default) where TEntity : class, new();
		void UpdateMany<TEntity>(IEnumerable<TEntity> entities) where TEntity : class, new();
		void Delete<TEntity>(TEntity entity) where TEntity : class, new();
		void Delete<TEntity>(Expression<Func<TEntity, bool>> where) where TEntity : class, new();
		Task DeleteAsync<TEntity>(Expression<Func<TEntity, bool>> where, CancellationToken token = default) where TEntity : class, new();
		void DeleteMany<TEntity>(IEnumerable<TEntity> entities) where TEntity : class, new();
	}
}