﻿namespace HS.Domain.Common
{
	using System;
	using System.Collections.Generic;
	using System.Linq.Expressions;
	using System.Threading;
	using System.Threading.Tasks;

    public interface IRepository
	{
		void Save();
		Task SaveAsync(CancellationToken token = default);
	}

	public interface IRepository<TEntity> : IRepository, IReadOnlyRepository<TEntity> where TEntity : class, new()
	{
		void Create(TEntity entity);
		Task CreateAsync(TEntity entity, CancellationToken token = default);
		void CreateMany(IEnumerable<TEntity> entities);
		Task CreateManyAsync(IEnumerable<TEntity> entities, CancellationToken token = default);
		void Update(TEntity entity);
		void Update(Expression<Func<TEntity, TEntity>> newValues, Expression<Func<TEntity, bool>> where = null);
		Task UpdateAsync(Expression<Func<TEntity, TEntity>> newValues, Expression<Func<TEntity, bool>> where = null, CancellationToken token = default);
		void UpdateMany(IEnumerable<TEntity> entities);
		void Delete(TEntity entity);
		void Delete(Expression<Func<TEntity, bool>> where);
		Task DeleteAsync(Expression<Func<TEntity, bool>> where, CancellationToken token = default);
		void DeleteMany(IEnumerable<TEntity> entities);
	}

	public interface IRepository<TKey, TEntity> : IRepository<TEntity> where TEntity : class, IEntity<TKey>, new()
	{
		TEntity Read(TKey id);
		Task<TEntity> ReadAsync(TKey id, CancellationToken token = default);
		void Delete(TKey id);
		Task DeleteAsync(TKey id, CancellationToken token = default);
	}
}