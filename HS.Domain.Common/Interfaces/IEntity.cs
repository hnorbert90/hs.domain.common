﻿namespace HS.Domain.Common
{
    public interface IEntity<TID>
    {
        TID Id { get; set; }
    }
}