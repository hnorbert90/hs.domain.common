﻿namespace HS.Domain.Common
{
    using System;
    using System.Linq;
    using System.Linq.Expressions;
    using System.Threading;
    using System.Threading.Tasks;

    public interface IReadOnlyRepository
	{
		TEntity Read<TEntity>(Expression<Func<TEntity, bool>> where) where TEntity : class, new();
		Task<TEntity> ReadAsync<TEntity>(Expression<Func<TEntity, bool>> where, CancellationToken token = default) where TEntity : class, new();
		IQueryable<TEntity> Query<TEntity>() where TEntity : class, new();
		IQueryable<TEntity> Query<TEntity>(Expression<Func<TEntity, bool>> filter) where TEntity : class, new();
		IQueryable<TEntity> Query<TEntity>(Expression<Func<TEntity, bool>> filter, Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy) where TEntity : class, new();
		long Count<TEntity>(Expression<Func<TEntity, bool>> filter = null) where TEntity : class, new();
		Task<long> CountAsync<TEntity>(Expression<Func<TEntity, bool>> filter = null, CancellationToken token = default) where TEntity : class, new();
	}

	public interface IReadOnlyRepository<TEntity> where TEntity : class, new()
	{
		IQueryable<TEntity> Set { get; }
		TEntity Read(Expression<Func<TEntity, bool>> where);
		Task<TEntity> ReadAsync(Expression<Func<TEntity, bool>> where, CancellationToken token = default);
		IQueryable<TEntity> Query();
		IQueryable<TEntity> Query(Expression<Func<TEntity, bool>> filter);
		IQueryable<TEntity> Query(Expression<Func<TEntity, bool>> filter, Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy);
		long Count(Expression<Func<TEntity, bool>> filter = null);
		Task<long> CountAsync(Expression<Func<TEntity, bool>> filter = null, CancellationToken token = default);
	}
}