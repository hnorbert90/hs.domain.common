﻿namespace HS.Domain.Common
{
    public interface ITrackableEntity<TUserID>
    {
        TUserID CreatedBy { get; set; }
        TUserID ModifiedBy { get; set; }
        TUserID DeletedBy { get; set; }
    }
}